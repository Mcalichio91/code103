

    let correoUsuario = document.getElementsByClassName("usuario")[0]
    let contraseñaUsuario = document.getElementsByClassName("contraseña")[0]

// function obtenerCorreoUsuario () {
//     console.log(correoUsuario.value, contraseñaUsuario.value)
// }


// Las constantes mantienen su valor a lo largo de la ejecucion del programa, de manera tal que no se
// puede renombrar ni cambiar su valor.

// const soyEstudiante = "Marcelo";

// Las variable de tipo let son variables dinamicas que me permiten almacenar y cambiar su valor
// a lo largo de la ejecucion del programa.

// let soyEstudiante = "Marcelo";


// let soyEstudiante = "Marcelo";


// console.log(soyEstudiante); // soyEstudiante is not defined


// function obtenerCorreoUsuario() {
//     soyEstudiante = "Pablo"
// }

// soyEstudiante = "Pablo"

// obtenerCorreoUsuario(); // true

// console.log(soyEstudiante);


function validarUsuario () {
    
    if(correoUsuario.value == "Marcelo" && contraseñaUsuario.value == "123456") {
        console.log("Usuario correcto")
        window.location = "/"
    } else {
        console.log("usuario o contraseña incorrectos")
    }
}